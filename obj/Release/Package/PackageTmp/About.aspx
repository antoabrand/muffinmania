﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="MuffinMania.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title%>.</h2>
    <h3>Who are we?</h3>
    <p>We make crazy muffins. 'nuff said.</p>
</asp:Content>
