﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="MuffinMania.ProductDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<asp:FormView ID="ProductDetail" runat="server"
    ItemType="MuffinMania.Models.Product"
    SelectMethod="GetProduct"
    RenderOuterTable="false">
    <ItemTemplate>
        <div>
            <h1><%#:Item.ProductName%></h1>
        </div>
        <br />
        <table>
            <tr>
                <td>
                    <img src="/Images/<%#:Item.ProductImage %>" 
                        style="border:groove; height:300px" alt="<%#:Item.ProductName %>"/>
                </td>          
                <td>&nbsp;</td>
                <td style="vertical-align:top; text-align:left;">
                    <b>Description:</b><br /><%#:Item.ProductDescription %>
                    <br />
                    <span><b>Price:</b>&nbsp;<%#:String.Format("{0:c}", Item.ProductPrice) %></span>
                    <br />

                    <span><b>Product Number:</b>&nbsp;<%#:Item.ProductId %></span>
                    <br />
                    <a
        href="AddToCart.aspx?productID=<%#:Item.ProductId %>">  
                                <span class="ProductListItem">
                                    <b>Add To Cart</b>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
</asp:Content>
