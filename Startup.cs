﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MuffinMania.Startup))]
namespace MuffinMania
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
