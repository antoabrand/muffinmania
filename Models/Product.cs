﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MuffinMania.Models
{
    public class Product
    {
        [ScaffoldColumn(false)]
        public int ProductId { get; set; }

        [Required, StringLength(100), Display(Name = "Name")]
        public string ProductName { get; set; }

        [Required, StringLength(1000), Display(Name = "Description"), 
        DataType(DataType.MultilineText)]
        public string ProductDescription { get; set; }

        public string ProductImage { get; set; }

        [Display(Name="Price")]
        public double? ProductPrice { get; set; }


        public int? CategoryId { get; set; }


        public virtual Category Category { get; set; }
    }
}