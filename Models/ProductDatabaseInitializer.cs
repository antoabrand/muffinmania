﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MuffinMania.Models
{
    public class ProductDatabaseInitializer : DropCreateDatabaseAlways<ProductContext>
    {

        protected override void Seed(ProductContext context)
        {
            getCategories().ForEach(c => context.Categories.Add(c)); //calls getCategories to fill the Db
            getProducts().ForEach(p => context.Products.Add(p));
        }

        //create a method of type list in order to get the categories 

        private static List<Category> getCategories()
        {
            var categories = new List<Category>
          {

           new Category
           {
            CategoryId=1,
            CategoryName = "Organic"
           },

           new Category
           {
            CategoryId=2,
            CategoryName = "Sugar Free"
           },

           new Category
           {
            CategoryId=3,
            CategoryName = "Savory"
           },

           new Category
           {
            CategoryId=4,
            CategoryName = "Magic Muffins"
           },

          };
            return categories;
        
        } //end of getCategories()

        public List<Product> getProducts() 
        {
            var products = new List<Product>
           {
             new Product
             {
                ProductId = 1,
                ProductName = "Organic Blueberry Blast",
                ProductDescription = "Made with real blueberrys and only the best, organic ingredients.",
                ProductImage = "blueBerryMuffin.jpg",
                ProductPrice = 19.99,
                CategoryId = 1
            
             },   
       
             new Product
             {
                ProductId = 2,
                ProductName = "Roaring Raison",
                ProductDescription = "Made with real roaring raisings and only the best, organic ingredients.",
                ProductImage = "organicRaisonMuffin.jpg",
                ProductPrice = 19.99,
                CategoryId = 1
            
             },

             new Product
             {
                ProductId = 3,
                ProductName = "Orange Burst",
                ProductDescription = "Made with real organges, grown in our very own orange groves, and only the best, sugar free organic ingredients.",
                ProductImage = "orangeMuffin.jpg",
                ProductPrice = 19.99,
                CategoryId = 2
            
             },

             new Product
             {
                ProductId = 4,
                ProductName = "Pear Ginger",
                ProductDescription = "Made with real pear and ginger, and only the best, sugar free organic ingredients.",
                ProductImage = "pearGinger.jpg",
                ProductPrice = 19.99,
                CategoryId = 2
            
             },

             new Product
             {
                ProductId = 5,
                ProductName = "The Savory Satisfier",
                ProductDescription = "Made with real tomatoes, spinach and chees, and only the best, organic ingredients.",
                ProductImage = "savoryMuffin.jpg",
                ProductPrice = 19.99,
                CategoryId = 3
            
             },

             new Product
             {
                ProductId = 6,
                ProductName = "Something Salty",
                ProductDescription = "Made with only the best, organic ingredients.",
                ProductImage = "saltyMuffin.jpg",
                ProductPrice = 19.99,
                CategoryId = 3
            
             },

             new Product
             {
                ProductId = 7,
                ProductName = "Magic Carpet Ride",
                ProductDescription = "Made with real cranberryies, infused with the finest, organic ingredients.(18+ and in select States)",
                ProductImage = "sliderMuffin.jpg",
                ProductPrice = 29.99,
                CategoryId = 4
            
             },

             new Product
             {
                ProductId = 8,
                ProductName = "Ain't Muffin Around",
                ProductDescription = "Made with real Blueberrys that will keep you coming back for more, and only the best, organic ingredients",
                ProductImage = "organicBlueBerry.jpg",
                ProductPrice = 29.99,
                CategoryId = 4
            
             }
           
           };
            return products;
        
        }
    }
}