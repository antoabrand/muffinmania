﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="MuffinMania.ShoppingCart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="ShoppingCartTitle" runat="server" class="ContentHead"><h1>Shopping Cart</h1></div>

    <asp:GridView ID="CartList" runat="server" AutoGenerateColumns="false"
        ShowFooter="true" GridLines="Vertical" CellPadding="4"
        ItemType="MuffinMania.Models.CartItem"
        SelectMethod="GetShoppingCartItems"
        CssClass="table table-striped table-bordered">
        <Columns>
            <asp:BoundField DataField="ProductId" HeaderText="ID"
                SortExpression="ProductId" />
            <asp:BoundField DataField="Product.ProductName" HeaderText ="Name" />
            <asp:BoundField DataField="Product.ProductPrice" HeaderText ="Price (each)" DataFormatString="{0:c}"/>
            <asp:TemplateField HeaderText="Quantity">
                <ItemTemplate>
                    <asp:TextBox ID="PurchaseQuantity" Width="40"
                        runat="server" Text="<%#:Item.Quantity %>"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Item Total">
                <ItemTemplate> <!--This shows the total for the amount of items times the amount of the itesm without taxes-->
                    <%#: String.Format("{0:c}", ((Convert.ToDouble(Item.Quantity)) *
                    Convert.ToDouble(Item.Product.ProductPrice)))%>
                </ItemTemplate>               
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Remove Item">
                <ItemTemplate>
                <asp:CheckBox id="Remove" runat="server"></asp:CheckBox>
                    </ItemTemplate>
            </asp:TemplateField>           
        </Columns>
    </asp:GridView>
    <div>
        <p></p>
        <strong>
            <asp:Label ID="LableTotalText" runat="server" Text="Order Total: "></asp:Label>
            <asp:Label ID="lblTotal" runat="server"
                EnableViewState="false"></asp:Label> 
        </strong>
    </div>
    <br />
    <table>
        <tr>
            <td>
                <asp:Button ID="UpdateBtn" runat="server" Text="Update"
                    OnClick ="UpdateBtn_Click" />
            </td>
            <td>
                <asp:ImageButton ID="CheckoutImageBtn" runat="server"
                    ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif"
                    Width="145" AlternateText="Check out with Paypal"
                    Onclick="CheckoutBtn_Click"
                    BackColor="Transparent" BorderWidth="0" />
            </td>
        </tr>
    </table>
</asp:Content>
