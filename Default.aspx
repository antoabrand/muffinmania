﻿<%@ Page Title="Welcome" Language="C#" MasterPageFile="~/Site.Master" 
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MuffinMania._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1><%: Title %></h1>
        <br />
        <h2>Come Get Crazy With Our Maniacal Muffins</h2>
        <p class="lead">Here at Muffin Mania, we believe that every muffin 
            we make has just the right amount of crazy. </p>
    </div>
   
</asp:Content>
