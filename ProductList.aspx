﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="MuffinMania.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section>
        <div>
            <hgroup>

                <h2>
                    <%: Page.Title %>
                </h2>

            </hgroup>

            <asp:ListView ID="productList" runat="server"
                DataKeyNames="ProductId" GroupItemCount="4"
                ItemType="MuffinMania.Models.Product" SelectMethod="GetProducts">

                <EmptyDataTemplate>
                    <table>
                        <tr>
                            <td>
                                No data returned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <EmptyItemTemplate>
                    <td />
                </EmptyItemTemplate>
                <GroupTemplate>
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </GroupTemplate>
                <ItemTemplate>
                    <td runat="server">
                        <table>
                        <tr>
                            <td>
                                <a
href="ProductDetails.aspx?productId=<%#:Item.ProductId %>">
                                    <img src="/Images/<%#:Item.ProductImage %>"
                                        width="100" height="75" style="border:groove" />
                                </a>
                            </td>
                        </tr>
                            <tr>
                        <td>
                            <a 
                                href="ProductDetails.aspx?productId=<%#:Item.ProductId %>">
                                <span>
                                    <%#:Item.ProductName %>
                                </span>
                                </a>
                            <br />
                            <span>
                                <b>Price: </b><%#:String.Format("{0:c}", 
                                              Item.ProductPrice)%>
                            </span>                           
                            <br />
                            <a
        href="AddToCart.aspx?productID=<%#:Item.ProductId %>">  
                                <span class="ProductListItem">
                                    <b>Add To Cart</b>
                                </span>
                            </a>
                        </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                            </table>
                        </p>
                    </td>
                </ItemTemplate>
                <LayoutTemplate>
                    <table style="width:100%;" >
                        <tbody>
                            <tr>
                                <td>
                                    <table id="groupPlaceholderContainer"
                                        runat="server" style="width:100%">
                                        <tr id="groupPlaceholder"></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr> 
                            <tr></tr>                        
                        </tbody>
                    </table>
                </LayoutTemplate>
            </asp:ListView>
        </div>
    </section>
</asp:Content>
