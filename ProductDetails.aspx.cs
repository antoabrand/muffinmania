﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MuffinMania.Models;
using System.Web.ModelBinding;

namespace MuffinMania
{
    public partial class ProductDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        public IQueryable<Product> GetProduct([QueryString("productId")] int? productId,[RouteData] string ProductName) 
        {
            var _db = new MuffinMania.Models.ProductContext();
            IQueryable<Product> query = _db.Products;
            if (productId.HasValue && productId > 0)
            {
                query = query.Where(p => p.ProductId == productId);
            }
            else if (!String.IsNullOrEmpty(ProductName))
            {
                query = query.Where(p =>
                          String.Compare(p.ProductName, ProductName) == 0);
            } 

            else { query = null; }

            return query; 
        }
    }
}